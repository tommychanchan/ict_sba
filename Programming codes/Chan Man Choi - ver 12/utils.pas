unit utils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, StdCtrls,
  ExtCtrls, Grids, dos, LCLType;

const
  ln = AnsiString(#13#10); //ln: line break
  max_item=1000;
  max_customer=3000;
  maxItemOnce=10; //max different number of items buy once
  maxTran=100000;
  itemFile='../../DB/items.txt';
  customerFile='../../DB/customer.txt';
  transactionFile='../../DB/transact.txt';
  settingFile='../../DB/settings.txt';
  purchaseFile='../../DB/purchase.txt';


//read data from data file: item, customer, transaction, setting
procedure readInData;

//save data to data file: item, customer, transaction, setting
procedure saveData;

//show data according to the query
procedure showItems(index:integer);

{
show the name, class and the money of user
return true if user exists
}
function showUserInfo(s:string):boolean;

//show the name, price and stock
procedure showItemInfo(s:string);

//find the index of the customer from its ID, return 0 if user not found
function customerIndex(s:string):integer;

//find the index of the item from its ID, return 0 if item not found
function itemIndex(s:string):integer;

//show the price of the corresponding item
procedure showPrice(index, n:integer);

//Add the buying item's information to the table and calculate the total price
procedure addToBrought(iIndex, quantity:integer; price: string);

//key press in page "buy"
procedure buyKeyPress(key:char);

//key press in page "supplier"
procedure supplierKeyPress(key:char);

//key press in page "addValue"
procedure addValueKeyPress(key:char);

//key press in page "writeOff"
procedure writeOffKeyPress(key:char);

//Add '0' before the string if not enough length
function addZero(s:string; num:integer):string;

//Generate the report of class
procedure reportPurchaseClass;

//get the class of the customer from userID
function classOf(id:string):string;

//Generate the report of gender
procedure reportPurchaseGender;

//Generate the report of top 10 purchase
procedure reportTopTenPurchase;

//Generate the report of top 10 frequency
procedure reportFrequency;

//get the new index of purchase.txt for saving
function getPurchaseCode:string;

//get today's date
function today:string;

//save the perchase detail to the data file
procedure savePurchase(purchaseCode:string; iIndex, quantity:integer);

//Cancel the buying
procedure cancelBuying;

//Format number with certain dp
function formatDp(s:string; dp:integer):string;



implementation
uses
  unit1, language;
//uses unit1: to access the variables and objects from the form
//uses language: to access langText


procedure readInData;
var
  f:text;
  i, count:integer;
  temp:char;
begin
  //item
  assign(f, itemFile);
  reset(f);
  i:=0;
  while not eof(f) do
  begin
    i:=i+1;
    read(f, item[i].itemCode);
    read(f, item[i].itemName);
    read(f, item[i].supplier);
    read(f, item[i].cost);
    read(f, item[i].price);
    readln(f, item[i].quantity);
  end; {of while}
  close(f);
  n_item:=i;

  //customer
  assign(f, customerFile);
  reset(f);
  i:=0;
  while not eof(f) do
  begin
    i:=i+1;
    read(f, customer[i].code);
    read(f, customer[i].cls);
    read(f, customer[i].clsNo);
    read(f, customer[i].name);
    read(f, customer[i].gender);
    read(f, temp);
    readln(f, customer[i].money);
  end; //of while
  close(f);
  n_customer:=i;

  //transaction
  assign(f, transactionFile);
  reset(f);
  i:=0;
  while not eof(f) do
  begin
    i:=i+1;
    read(f, transaction[i].tranId);
    read(f, temp);
    read(f, transaction[i].tranDate);
    read(f, temp);
    read(f, transaction[i].customerID);
    read(f, transaction[i].tranAmount);
    read(f, temp);
    count:=0;
    while not eoln(f) do
    begin
      count:=count+1;
      read(f, transaction[i].tranDetailItem[count].itemCode);
      read(f, temp);
      read(f, transaction[i].tranDetailItem[count].quantity);
      read(f, temp);
    end; //of while
    transaction[i].n_detail:=count;
    readln(f);
  end; //of while
  close(f);
  n_transaction:=i;

  assign(f, settingFile);
  reset(f);
  readln(f, lang); //language
  close(f);
end; //of procedure

procedure saveData;
var
  f:text;
  i, j:integer;
  temp:char=' ';
begin
  //item
  assign(f, itemFile);
  rewrite(f);
  for i:=1 to n_item do
  begin
    write(f, item[i].itemCode);
    write(f, item[i].itemName);
    write(f, item[i].supplier);
    write(f, item[i].cost:5:1);
    write(f, item[i].price:5:1);
    writeln(f, item[i].quantity:3);
  end; //of for
  close(f);

  //customer
  assign(f, customerFile);
  rewrite(f);
  for i:=1 to n_customer do
  begin
    write(f, customer[i].code);
    write(f, customer[i].cls);
    write(f, customer[i].clsNo);
    write(f, customer[i].name);
    write(f, customer[i].gender);
    writeln(f, customer[i].money:6:1);
  end; //of for
  close(f);

  //transaction
  assign(f, transactionFile);
  rewrite(f);
  for i:=1 to n_transaction do
  begin
    write(f, transaction[i].tranId);
    write(f, temp);
    write(f, transaction[i].tranDate);
    write(f, temp);
    write(f, transaction[i].customerID);
    write(f, transaction[i].tranAmount:7:1);
    write(f, temp);
    for j:=1 to transaction[i].n_detail do
    begin
      write(f, transaction[i].tranDetailItem[j].itemCode);
      write(f, temp);
      write(f, transaction[i].tranDetailItem[j].quantity);
      write(f, temp);
    end; //of for
    writeln(f);
  end; //of for
  close(f);

  assign(f, settingFile);
  rewrite(f);
  writeln(f, lang); //language
  close(f);
end; //of procedure

procedure showItems(index:integer);
var
  letter:char;
  i, count:integer;
begin
  //reset the table
  mainForm.enquiry.Clean;
  mainForm.enquiry.RowCount:=1;

  case index of
    0:letter:='S';
    1:letter:='R';
    2:letter:='F';
    3:letter:='D';
  end; //of case

  count:=0;
  for i:=1 to n_item do
    if item[i].itemCode[1]=letter then
    begin
      count:=count+1;
      mainForm.enquiry.RowCount:=mainForm.enquiry.RowCount+1;
      mainForm.enquiry.Cells[0, count]:=item[i].itemCode;
      mainForm.enquiry.Cells[1, count]:=item[i].itemName;
      mainForm.enquiry.Cells[2, count]:=floatToStr(item[i].price);
      mainForm.enquiry.Cells[3, count]:=intToStr(item[i].quantity);
    end; //of if


end; //of procedure

function showUserInfo(s:string):boolean;
var
  index:integer;
  re:boolean;
begin
  index:=customerIndex(s);
  re:=false;
  if index<>0 then
  begin
    //user found
    mainForm.customerNameText.Caption:=langText[lang].welcome+', '+customer[index].name; //Show name

    if customer[index].cls='SS' then
      mainForm.customerNameText.Caption:=
      mainForm.customerNameText.Caption+'('+langText[lang].staff+')'
    else
      mainForm.customerNameText.Caption:=
      mainForm.customerNameText.Caption+'('+customer[index].cls+')';

    mainForm.customerNameText.Caption:=
    mainForm.customerNameText.Caption+ln+langText[lang].money+' $'+
    formatDp(floatToStr(customer[index].money), 1); //Show Money

    re:=true;
  end; //of if
  showUserInfo:=re;
end; //of function

procedure showItemInfo(s:string);
var
  index:integer;
begin
  index:=itemIndex(s);
  if index<>0 then
  begin
    //item found
    mainForm.itemNameText.Caption:=item[index].itemName+' ('+
                                   intToStr(item[index].quantity)+')';
    mainForm.itemPriceOne.Caption:='$'+floatToStr(item[index].price);
  end; //of if
end; //of procedure

function customerIndex(s:string):integer;
var
  i, re:integer;
begin
  s:=upperCase(s);
  re:=0; //return 0 if not found
  for i:=1 to n_customer do
    if customer[i].code=s then
    begin
      re:=i;
      break; //exit for loop
    end; //of if
  customerIndex:=re;
end; //of function

function itemIndex(s:string):integer;
var
  i, re:integer;
begin
  s:=upperCase(s);
  re:=0; //return 0 if not found
  for i:=1 to n_item do
    if item[i].itemCode=s then
    begin
      re:=i;
      break; //exit for loop
    end; //of if
  itemIndex:=re;
end; //of function

procedure showPrice(index, n:integer);
begin
  mainForm.priceText.Caption:='$'+floatToStr(item[index].price*n);
end; //of procedure

procedure addToBrought(iIndex, quantity:integer; price: string);
var
  row, i, j:integer;
  sum:real;
  added:boolean;
begin
  row:=mainForm.brought.RowCount;
  added:=false; //ini
  for i:=1 to row-1 do
    if item[iIndex].itemCode=mainForm.brought.Cells[1, i] then
    begin
      //repeated item
      mainForm.brought.Cells[2, i]:=
      intToStr(strToInt(mainForm.brought.Cells[2, i])+
      quantity); //add quantity

      mainForm.brought.Cells[4, i]:=
      floatToStr(strToFloat(mainForm.brought.Cells[4, i])+
      strToFloat(price)); //add price

      added:=true;
      sum:=0;

      //calculate new total price
      for j:=1 to row-1 do
        sum:=sum+strToFloat(mainForm.brought.Cells[4, j]);

      //add to transaction
      for j:=1 to detailCount do
        if detail[j].itemCode=item[iIndex].itemCode then
          detail[j].quantity:=detail[j].quantity+quantity;

    end; //of if

  if not added then
  begin
    //not repeat item
    mainForm.brought.RowCount:=row+1;

    //add to table
    mainForm.brought.Cells[0, row]:=intToStr(row);
    mainForm.brought.Cells[1, row]:=item[iIndex].itemCode;
    mainForm.brought.Cells[2, row]:=intToStr(quantity);
    mainForm.brought.Cells[3, row]:=item[iIndex].itemName;
    mainForm.brought.Cells[4, row]:=price;
    sum:=0;

    for j:=1 to row do
      sum:=sum+strToFloat(mainForm.brought.Cells[4, j]);

    //add to transaction
    detailCount:=detailCount+1;
    detail[detailCount].itemCode:=item[iIndex].itemCode;
    detail[detailCount].quantity:=quantity;
  end; //of if


  //show the total price and enable delete button and confirm button
  mainForm.totalPriceText.Caption:='Total: $'+floatToStr(sum);
  mainForm.confirmBt.Enabled:=mainForm.brought.RowCount>1;
end; //of procedure

procedure buyKeyPress(key:char);
begin
  case ord(key) of
    27:cancelBuying; //Esc
    13:
    begin //Enter
      if mainForm.IDInput.Text='' then
        mainForm.IDInput.setFocus
      else if ((mainForm.quantityInput.Text='') or
              (mainForm.quantityInput.Text='0')) and
              (mainForm.quantityInput.Enabled) then
        mainForm.quantityInput.setFocus
      else if (mainForm.itemCodeInput.Focused) and
              (mainForm.quantityInput.Enabled) then
        mainForm.quantityInput.SetFocus
      else if (mainForm.itemCodeInput.Text='') and
              (mainForm.IDInput.Text<>'') and
              (mainForm.itemCodeInput.Focused) and
              (mainForm.confirmBt.Enabled) then
        mainForm.confirmBtClick(nil)
      else if (mainForm.itemCodeInput.Text='') and
              (mainForm.itemCodeInput.Enabled) then
        mainForm.itemCodeInput.setFocus
      else
        mainForm.addItemBtClick(nil);
    end;
  end; //of case
end; //of procedure

procedure supplierKeyPress(key:char);
begin
  case ord(key) of
    27:
    begin //Esc
      mainForm.itemCodePurchase.Text:='';
      mainForm.quantityPurchase.Text:='0';
      mainForm.confirmPurchaseBt.Enabled:=false;
      mainForm.itemCodePurchase.setFocus;
    end;
    13:
    begin //Enter
      if mainForm.itemCodePurchase.Text='' then
        mainForm.itemCodePurchase.setFocus
      else if ((mainForm.quantityPurchase.Text='') or
              (mainForm.quantityPurchase.Text='0')) and
              (mainForm.quantityPurchase.Enabled) then
        mainForm.quantityPurchase.setFocus
      else
        mainForm.confirmPurchaseBtClick(nil);
    end;
  end; //of case
end; //of procedure

procedure addValueKeyPress(key:char);
begin
  case ord(key) of
    27:
    begin //Esc
      mainForm.IDInputAddValue.Text:='';
      mainForm.moneyAddValue.Text:='0';
    end;
    13:
    begin //Enter
      if mainForm.IDInputAddValue.Text='' then
        mainForm.IDInputAddValue.setFocus
      else if ((mainForm.moneyAddValue.Text='') or
              (mainForm.moneyAddValue.Text='0')) and
              (mainForm.moneyAddValue.Enabled) then
        mainForm.moneyAddValue.setFocus
      else if mainForm.confirmAddValue.Enabled then
        mainForm.confirmAddValueClick(nil);
    end;
  end; //of case
end; //of procedure

procedure writeOffKeyPress(key:char);
begin
  case ord(key) of
    27:
    begin //Esc
      mainForm.itemCodeWriteOffInput.Text:='';
      mainForm.amountWriteOff.Text:='0';
    end;
    13:
    begin //Enter
      if mainForm.itemCodeWriteOffInput.Text='' then
        mainForm.itemCodeWriteOffInput.setFocus
      else if ((mainForm.amountWriteOff.Text='') or
              (mainForm.amountWriteOff.Text='0')) and
              (mainForm.amountWriteOff.Enabled) then
        mainForm.amountWriteOff.setFocus
      else if mainForm.confirmWriteOff.Enabled then
        mainForm.confirmWriteOffClick(nil);
    end;
  end; //of case
end; //of procedure

function addZero(s:string; num:integer):string;
var
  re:string;
begin
  re:=s;
  while length(re)<num do
    re:='0'+re;
  addZero:=re;
end; //of function

procedure reportPurchaseClass;
type
  classesType=array [1..13] of string;
  classesPurchaseType=array [1..13] of real;
var
  classes:classesType;
  classesPurchase:classesPurchaseType;
  i, classID:integer;
  classTemp:string;
  f:text;
begin
  classes[1]:='1A';
  classes[2]:='1B';
  classes[3]:='1C';
  classes[4]:='1D';
  classes[5]:='2A';
  classes[6]:='2B';
  classes[7]:='2C';
  classes[8]:='2D';
  classes[9]:='3A';
  classes[10]:='3B';
  classes[11]:='3C';
  classes[12]:='3D';
  classes[13]:='Staff';
  for i:=1 to 13 do
    classesPurchase[i]:=0; //ini

  for i:=1 to n_transaction do
  begin
    //get transaction
    classTemp:=classOf(transaction[i].customerID); //1A/1B/1C...
    case classTemp of
      '1A':classID:=1;
      '1B':classID:=2;
      '1C':classID:=3;
      '1D':classID:=4;
      '2A':classID:=5;
      '2B':classID:=6;
      '2C':classID:=7;
      '2D':classID:=8;
      '3A':classID:=9;
      '3B':classID:=10;
      '3C':classID:=11;
      '3D':classID:=12;
      'SS':classID:=13;
    end; //of case

    //count amount of buying of each class
    classesPurchase[classID]:=classesPurchase[classID]+transaction[i].tranAmount;
  end; //of for

  //generate report
  assign(f, 'amount_of_purchase_by_class.txt');
  rewrite(f);
  writeln(f, ' Class    Amount of purchase');
  writeln(f, '----------------------------');
  for i:=1 to 13 do
  begin
    write(f, '  ', classes[i], '':15-length(classes[i]));
    writeln(f, '$ ', classesPurchase[i]:0:1);
  end; //of for
  close(f);

  //open report
  exec('cmd.exe', '/C start "" notepad.exe amount_of_purchase_by_class.txt');
  //user can continue using the app while the notepad is opening
end; //of procedure

function classOf(id:string):string;
begin
  classOf:=customer[customerIndex(id)].cls; //get the class of customer
end; //of function

procedure reportPurchaseGender;
type
  genderType=array [1..2] of string;
  genderPurchaseType=array [1..2] of real;
var
  gender:genderType;
  genderPurchase:genderPurchaseType;
  i, genderID:integer;
  genderTemp:string;
  f:text;
begin
  gender[1]:='F';
  gender[2]:='M';
  genderPurchase[1]:=0;
  genderPurchase[2]:=0;

  for i:=1 to n_transaction do
  begin
    //get transaction
    genderTemp:=copy(customer[customerIndex(transaction[i].customerID)].gender, 1, 1); //F/M
    case genderTemp of
      'F':genderID:=1;
      'M':genderID:=2;
    end; //of case

    //count amount of buying of each gender
    genderPurchase[genderID]:=genderPurchase[genderID]+transaction[i].tranAmount;
  end; //of for

  //generate report
  assign(f, 'amount_of_purchase_by_gender.txt');
  rewrite(f);
  writeln(f, ' Gender   Amount of purchase');
  writeln(f, '----------------------------');
  for i:=1 to 2 do
  begin
    write(f, '  ', gender[i], '':10);
    writeln(f, '$ ', genderPurchase[i]:0:1);
  end; //of for
  close(f);

  //open report
  exec('cmd.exe', '/C start "" notepad.exe amount_of_purchase_by_gender.txt');
  //user can continue using the app while the notepad is opening
end; //of procedure

procedure reportTopTenPurchase;
type
  purchaseType=array [1..max_customer] of real;

  function findMost(purchase:purchaseType):integer;
  //find the highest purchase amount, return index
  var
    re, i:integer;
  begin
    re:=1;
    for i:=2 to n_customer do
      if purchase[i]>purchase[re] then
        re:=i;
    findMost:=re;
  end; //of function


var
  i, index:integer;
  purchase:purchaseType;
  f:text;
begin
  for i:=1 to n_customer do
    purchase[i]:=0;
  for i:=1 to n_transaction do
  begin
    //get transaction
    index:=customerIndex(transaction[i].customerID);

    //count amount of buying
    if index<>0 then //to prevent crash
      purchase[index]:=purchase[index]+transaction[i].tranAmount;
  end; //of for

  //generate report
  assign(f, 'top_10_students_of_most_purchasing_amount.txt');
  rewrite(f);
  writeln(f, 'Class   Class no.   Name               Purchasing amount');
  writeln(f, '--------------------------------------------------------');
  for i:=1 to 10 do
  begin
    index:=findMost(purchase);
    write(f, customer[index].cls, '':8, customer[index].clsNo, '':8);
    writeln(f, customer[index].name, '$', purchase[index]:0:1);

    //reset the largest purchase amount
    purchase[index]:=0;
  end; //of for
  close(f);

  //open report
  exec('cmd.exe', '/C start "" notepad.exe top_10_students_of_most_purchasing_amount.txt');
  //user can continue using the app while the notepad is opening
end; //of procedure

procedure reportFrequency;
type
  frequencyType=array [1..max_customer] of integer;

  function findMost(frequency:frequencyType):integer;
  //find the highest purchase frequency, return index
  var
    re, i:integer;
  begin
    re:=1;
    for i:=2 to n_customer do
      if frequency[i]>frequency[re] then
        re:=i;
    findMost:=re;
  end; //of function

var
  index, i:integer;
  frequency:frequencyType;
  f:text;
begin
  for i:=1 to n_customer do
    frequency[i]:=0;
  for i:=1 to n_transaction do
  begin
    index:=customerIndex(transaction[i].customerID);
    if index<>0 then //to prevent crash
      frequency[index]:=frequency[index]+1;
  end; //of for

  //generate report
  assign(f, 'top_10_students_of_most_purchasing_frequency.txt');
  rewrite(f);
  writeln(f, 'Class   Class no.   Name               Frequency');
  writeln(f, '------------------------------------------------');
  for i:=1 to 10 do
  begin
    index:=findMost(frequency);
    write(f, customer[index].cls, '':8, customer[index].clsNo, '':8);
    writeln(f, customer[index].name, frequency[index]);

    //reset the largest Frequency amount
    frequency[index]:=0;
  end; //of for
  close(f);

  //open report
  exec('cmd.exe', '/C start "" notepad.exe top_10_students_of_most_purchasing_frequency.txt');
  //user can continue using the app while the notepad is opening


end; //of procedure

function getPurchaseCode:string;
var
  f:text;
  i:integer;
  re:string;
begin
  re:='';
  i:=1; //normally should be 0, but the new index should add 1.
  assign(f, purchaseFile);
  reset(f);
  while not eof(f) do
  begin
    i:=i+1;
    readln(f);
  end; //of while
  close(f);

  re:=addZero(intToStr(i), 4);
  getPurchaseCode:=re;
end; //of function

function today:string;
var
  year,month,day,wDay:word;
  re:string;
begin
  GetDate(year,month,day,wDay);
  re:=intToStr(year)+addZero(intToStr(month), 2)+addZero(intToStr(day), 2);
  today:=re;
end; //of function

procedure savePurchase(purchaseCode:string; iIndex, quantity:integer);
var
  cost:real;
  f:text;
begin
  //calculate the cost
  cost:=item[iIndex].cost*quantity;

  assign(f, purchaseFile);
  append(f);
  write(f, purchaseCode+' ');
  write(f, today);
  write(f, cost:7:1);
  write(f, ' ');
  write(f, item[iIndex].itemCode+' ');
  write(f, quantity:3);
  writeln(f, ' ');
  close(f);
  showMessage(langText[lang].successMsg);
end; //of procedure

procedure cancelBuying;
begin
  readInData;

  detailCount:=0; //reset detail

  mainForm.IDInput.Text:='';
  mainForm.itemCodeInput.Text:='';
  mainForm.quantityInput.Text:='1';
  mainForm.quantityInput.Enabled:=false;
  mainForm.itemNameText.Caption:='';
  mainForm.priceText.Caption:='';
  mainForm.itemPriceOne.Caption:='';
  mainForm.confirmBt.Enabled:=false;
  mainForm.IDInput.Enabled:=true;
  mainForm.brought.RowCount:=1;
  mainForm.brought.Clean;
  mainForm.totalPriceText.Caption:='';
  mainForm.IDInput.setFocus;
end; //of procedure

function formatDp(s:string; dp:integer):string;
var
  re:string;
  p:integer;
begin
  re:='';
  p:=pos('.', s);
  if p=0 then // '.' not found
    re:=s
  else
    re:=copy(s, 1, p+dp);
  formatDp:=re;
end; //of function






end.

