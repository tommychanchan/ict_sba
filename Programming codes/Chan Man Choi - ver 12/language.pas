unit language;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;


//load the corresponding language
procedure loadLanguage;

//ini language words
procedure iniLanguage;


type
  languageText=object
    title,
    customerID,
    itemCode,
    language,
    itemName,
    itemPrice,
    quantityQ,
    addItem,
    confirm,
    cancel,
    sandwich,
    rice,
    snackAndFruit,
    drink,
    reportClass,
    reportGender,
    reportTopAmount,
    reportTopFreq,
    generage,
    amount,
    moneyToAdd,
    amountToWriteOff,
    enquiryPage,
    buyPage,
    reportsPage,
    purchasePage,
    addValuePage,
    writeOffPage,
    settingPage,
    notEnoughMoneyMsg,
    youNowHaveMsg,
    itCostsMsg,
    notEnoughGoodsMsg,
    weNowHaveMsg,
    maxNumberOnceMsg,
    itemCodeErrMsg,
    customerIDErrMsg,
    moneyToAddMsg,
    writeOffMsg,
    tooMuchAmountMsg,
    successMsg,
    userName,
    staff,
    money,
    addValueMost,
    totalCost,
    itemQuantityInStock,
    writeOffAtMost,
    full,
    supplier,
    cannotBuy,
    canBuy,
    purchaseCostPerItem,
    changePageErrMsg,
    adminMsg,
    atLeastOneMsg,
    welcome,
    code,
    name,
    price,
    quantity:string;
  end; //of object


var
  langText:array [0..1] of languageText;


implementation
uses
  Unit1;




procedure loadLanguage;
begin
  with mainForm do
  begin
    Caption:=langText[lang].title;
    customerIDLabel.Caption:=langText[lang].customerID;
    customerIDAddValueLabel.Caption:=langText[lang].customerID;
    itemCodeLabel.Caption:=langText[lang].itemCode;
    itemCodePurchaseLabel.Caption:=langText[lang].itemCode;
    itemCodeWriteOffLabel.Caption:=langText[lang].itemCode;
    languageLabel.Caption:=langText[lang].language;
    itemNameLabel.Caption:=langText[lang].itemName;
    itemPriceLabel.Caption:=langText[lang].itemPrice;
    quantityInputLabel.Caption:=langText[lang].quantityQ;
    addItemBt.Caption:=langText[lang].addItem;
    confirmBt.Caption:=langText[lang].confirm;
    confirmPurchaseBt.Caption:=langText[lang].confirm;
    confirmAddValue.Caption:=langText[lang].confirm;
    confirmWriteOff.Caption:=langText[lang].confirm;
    cancelBt.Caption:=langText[lang].cancel;

    select.Items.Clear; //reset
    select.Items.Add(langText[lang].sandwich);
    select.Items.Add(langText[lang].rice);
    select.Items.Add(langText[lang].snackAndFruit);
    select.Items.Add(langText[lang].drink);

    reportList.Items.Clear; //reset
    reportList.Items.Add(langText[lang].reportClass);
    reportList.Items.Add(langText[lang].reportGender);
    reportList.Items.Add(langText[lang].reportTopAmount);
    reportList.Items.Add(langText[lang].reportTopFreq);

    reportBt.Caption:=langText[lang].generage;
    amountPurchaseLabel.Caption:=langText[lang].amount;
    moneyAddValueLabel.Caption:=langText[lang].moneyToAdd;
    amountWriteOffLabel.Caption:=langText[lang].amountToWriteOff;
    pcItems.Caption:=langText[lang].enquiryPage;
    buy.Caption:=langText[lang].buyPage;
    reports.Caption:=langText[lang].reportsPage;
    supplier.Caption:=langText[lang].purchasePage;
    addValue.Caption:=langText[lang].addValuePage;
    writeOff.Caption:=langText[lang].writeOffPage;
    setting.Caption:=langText[lang].settingPage;

    enquiry.Columns[0].Title.Caption:=langText[lang].code;
    enquiry.Columns[1].Title.Caption:=langText[lang].name;
    enquiry.Columns[2].Title.Caption:=langText[lang].price;
    enquiry.Columns[3].Title.Caption:=langText[lang].quantity;

    brought.Columns[1].Title.Caption:=langText[lang].code;
    brought.Columns[2].Title.Caption:=langText[lang].quantity;
    brought.Columns[3].Title.Caption:=langText[lang].name;
    brought.Columns[4].Title.Caption:=langText[lang].price;

    //call onchange event so that reload text
    IDInputChange(nil);
    itemCodeInputChange(nil);
    itemCodePurchaseChange(nil);
    IDInputAddValueChange(nil);
    itemCodeWriteOffInputChange(nil);
  end; //of with

  //reset select index to 0
  mainForm.select.ItemIndex:=0;
  mainForm.reportList.ItemIndex:=0;
end; //of procedure

procedure iniLanguage;
begin
  //English
  with langText[0] do
  begin
    title:='POS System of a School Tuck Shop';
    customerID:='Customer ID:';
    itemCode:='Item Code:';
    language:='Language:';
    itemName:='Item Name:';
    itemPrice:='Item Price:';
    quantityQ:='How many do you want to buy?';
    addItem:='Add Item (Enter)';
    confirm:='Confirm';
    cancel:='Cancel (Esc)';
    sandwich:='Sandwich';
    rice:='Rice';
    snackAndFruit:='Snack and Fruit';
    drink:='Drink';
    reportClass:='Amount of purchase by class';
    reportGender:='Amount of purchase by gender';
    reportTopAmount:='Top 10 students of most purchasing amount';
    reportTopFreq:='Top 10 students of most purchasing frequency';
    generage:='Generate';
    amount:='Amount:';
    moneyToAdd:='Money to add:';
    amountToWriteOff:='Amount to write off:';
    enquiryPage:='Enquiry';
    buyPage:='Buy';
    reportsPage:='Reports';
    purchasePage:='Purchase from supplier';
    addValuePage:='Add Value';
    writeOffPage:='Write Off';
    settingPage:='Setting';
    notEnoughMoneyMsg:='You have not enough money to buy.';
    youNowHaveMsg:='You now have';
    itCostsMsg:='It costs';
    notEnoughGoodsMsg:='Not enough goods in stock.';
    weNowHaveMsg:='We now have';
    maxNumberOnceMsg:='Excess max number of different items once.';
    itemCodeErrMsg:='Item code not found.';
    customerIDErrMsg:='Customer ID not found.';
    moneyToAddMsg:='Money to add must be greater than 0.';
    writeOffMsg:='Amount must be greater than 0.';
    tooMuchAmountMsg:='Too much. It must be less than or equal to';
    successMsg:='Success!';
    userName:='User Name:';
    staff:='Staff';
    money:='Money:';
    addValueMost:='Add value at most:';
    totalCost:='Total cost:';
    itemQuantityInStock:='Item quantity in stock:';
    writeOffAtMost:='You can write off at most';
    full:='Full';
    supplier:='Supplier:';
    cannotBuy:='You cannot buy it';
    canBuy:='You can buy:';
    purchaseCostPerItem:='Purchase cost per item:';
    changePageErrMsg:='You must confirm your buying or cancel it before changing page.';
    adminMsg:='Something wrong! Please contact the administrator.';
    atLeastOneMsg:='You must buy at least one.';
    welcome:='Welcome';
    code:='Code';
    name:='Name';
    price:='Price ($)';
    quantity:='Quantity';
  end; //of with



  //Chinese
  with langText[1] do
  begin
    title:='學校小賣部銷售系統';
    customerID:='用戶 ID：';
    itemCode:='物品編號：';
    language:='語言：';
    itemName:='物品名稱：';
    itemPrice:='物品價錢：';
    quantityQ:='你想買多少件貨品？';
    addItem:='新增物品 (Enter)';
    confirm:='確定';
    cancel:='取消 (Esc)';
    sandwich:='三文治';
    rice:='飯';
    snackAndFruit:='小食和水果';
    drink:='飲品';
    reportClass:='各班別購買金錢';
    reportGender:='各性別購買金錢';
    reportTopAmount:='頭10購買金錢';
    reportTopFreq:='頭10購買次數';
    generage:='保存並開啟';
    amount:='數量：';
    moneyToAdd:='增值額：';
    amountToWriteOff:='註銷數量：';
    enquiryPage:='查詢';
    buyPage:='購買';
    reportsPage:='報告';
    purchasePage:='向供應商購買';
    addValuePage:='增值';
    writeOffPage:='註銷';
    settingPage:='設定';
    notEnoughMoneyMsg:='你沒有足夠金錢購買。';
    youNowHaveMsg:='你現在有';
    itCostsMsg:='它價格是';
    notEnoughGoodsMsg:='存貨不足。';
    weNowHaveMsg:='現在有';             
    maxNumberOnceMsg:='一次購買過多類型貨品。';
    itemCodeErrMsg:='物品編號不存在。';
    customerIDErrMsg:='用戶ID不存在。';
    moneyToAddMsg:='增值數目必須大於0。';
    writeOffMsg:='數量必須大於0。';
    tooMuchAmountMsg:='太多了。他必須少於或等於';
    successMsg:='成功！';
    userName:='用戶名：';
    staff:='職員';
    money:='金錢：';
    addValueMost:='增值金額上限：';
    totalCost:='合計價錢：';
    itemQuantityInStock:='存貨量：';
    writeOffAtMost:='你最多能夠註銷';
    full:='已滿';
    supplier:='供應商：';
    cannotBuy:='你不能夠購買它';
    canBuy:='你可以購買：';
    purchaseCostPerItem:='成本價(一件)：';
    changePageErrMsg:='更改頁面前你必須確定或取消購買交易。';
    adminMsg:='錯誤！請聯絡管理員。';
    atLeastOneMsg:='你最少需要購買一件。';
    welcome:='歡迎';
    code:='編碼';
    name:='名稱';
    price:='價格 ($)';
    quantity:='數量';
  end; //of with
end; //of procedure






end.

