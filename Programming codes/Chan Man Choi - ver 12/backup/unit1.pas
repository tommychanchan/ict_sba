unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, StdCtrls,
  ExtCtrls, Grids, utils, Math, dos, language;


type

  { TmainForm }

  TmainForm=class(TForm)
    addItemBt:TButton;
    confirmWriteOff: TButton;
    amountWriteOff: TEdit;
    itemCodeWriteOffInput: TEdit;
    itemCodeWriteOffLabel: TLabel;
    itemInfoWriteOff: TLabel;
    amountWriteOffLabel: TLabel;
    language: TComboBox;
    confirmAddValue: TButton;
    confirmPurchaseBt:TButton;
    costPurchase:TLabel;
    languageLabel: TLabel;
    moneyAddValueLabel: TLabel;
    moneyAddValue: TEdit;
    IDInputAddValue: TEdit;
    setting: TTabSheet;
    writeOff: TTabSheet;
    userInfoAddValue: TLabel;
    customerIDAddValueLabel: TLabel;
    quantityPurchase:TEdit;
    itemCodePurchase:TEdit;
    itemCodePurchaseLabel:TLabel;
    itemInfoPurchase:TLabel;
    amountPurchaseLabel:TLabel;
    reportBt:TButton;
    reportList:TComboBox;
    cancelBt:TButton;
    confirmBt:TButton;
    itemPriceLabel:TLabel;
    itemPriceOne:TLabel;
    reports:TTabSheet;
    supplier:TTabSheet;
    addValue: TTabSheet;
    totalPriceText:TLabel;
    priceText:TLabel;
    quantityInput:TEdit;
    itemCodeInput:TEdit;
    IDInput:TEdit;
    customerIDLabel:TLabel;
    itemCodeLabel:TLabel;
    itemNameLabel:TLabel;
    itemNameText:TLabel;
    customerNameText:TLabel;
    quantityInputLabel:TLabel;
    select:TComboBox;
    pc:TPageControl;
    pcItems:TTabSheet;
    enquiry:TStringGrid;
    buy:TTabSheet;
    brought:TStringGrid;
    procedure addItemBtClick(Sender:TObject);
    procedure amountWriteOffKeyPress(Sender: TObject; var Key: char);
    procedure buyShow(Sender:TObject);
    procedure cancelBtClick(Sender:TObject);
    procedure confirmAddValueClick(Sender: TObject);
    procedure confirmBtClick(Sender:TObject);
    procedure confirmPurchaseBtClick(Sender:TObject);
    procedure confirmWriteOffClick(Sender: TObject);
    procedure deleteBtClick(Sender:TObject);
    procedure deleteItemIndexKeyPress(Sender:TObject; var Key:char);
    procedure IDInputAddValueChange(Sender: TObject);
    procedure IDInputAddValueKeyPress(Sender: TObject; var Key: char);
    procedure itemCodeInputKeyPress(Sender:TObject; var Key:char);
    procedure itemCodePurchaseKeyPress(Sender:TObject; var Key:char);
    procedure itemCodeWriteOffInputChange(Sender: TObject);
    procedure itemCodeWriteOffInputKeyPress(Sender: TObject; var Key: char);
    procedure languageChange(Sender: TObject);
    procedure moneyAddValueKeyPress(Sender: TObject; var Key: char);
    procedure quantityInputKeyPress(Sender:TObject; var Key:char);
    procedure quantityPurchaseChange(Sender:TObject);
    procedure itemCodePurchaseChange(Sender:TObject);
    procedure FormActivate(Sender:TObject);
    procedure FormResize(Sender:TObject);
    procedure IDInputChange(Sender:TObject);
    procedure IDInputKeyPress(Sender:TObject; var Key:char);
    procedure itemCodeInputChange(Sender:TObject);
    procedure pcChange(Sender:TObject);
    procedure quantityInputChange(Sender:TObject);
    procedure quantityPurchaseKeyPress(Sender:TObject; var Key:char);
    procedure reportBtClick(Sender:TObject);
    procedure selectChange(Sender:TObject);
  private

  public

  end;

  string2=string[2];
  string4=string[4];
  string8=string[8];
  string20=string[20];
  string25=string[25];

  item_record=record
    itemCode:string4;
    itemName:string25;
    supplier:string25;
    cost:real;
    price:real;
    quantity:integer;
  end; //of record

  customer_record=record
    code:string8;
    cls:string2;
    clsNo:string2;
    name:string20;
    gender:string2;
    money:real;
  end; //of record

  tran_detail=record
    itemCode:string4;
    quantity:integer;
  end; //of record

  tran_detail_array=array [1..maxItemOnce] of tran_detail;

  transaction_record=record
    tranId:string4;
    tranDate:string8;
    customerID:string8;
    tranAmount:real;
    n_detail:integer;
    tranDetailItem:tran_detail_array;
  end; //of record


  item_array=array [1..max_item] of item_record;
  customer_array=array [1..max_customer] of customer_record;
  transaction_array=array [1..maxTran] of transaction_record;



var
  mainForm: TMainForm;
  item:item_array;
  customer:customer_array;
  n_item, n_customer, detailCount:integer;
  //detailCount = number of different items user brought
  detail:tran_detail_array;
  transaction:transaction_array;
  n_transaction:integer;
  lang:integer;



implementation

{$R *.lfm}

{ TmainForm }

procedure TmainForm.FormActivate(Sender:TObject);
begin
  readInData; //get info in database

  iniLanguage;

  language.ItemIndex:=lang;

  loadLanguage;

  //ini form elements setting
  showItems(select.itemIndex);
  quantityInput.Enabled:=false;
  confirmBt.Enabled:=false;

  detailCount:=0; //ini detail

  pc.ActivePage:=buy; //set ActivePage for page control
  IDInput.setFocus; //focus IDInput
end;

procedure TmainForm.addItemBtClick(Sender:TObject);
var
  cIndex, iIndex, //cIndex=customer index; iIndex=item index.
  quantity, i:integer;
  price:real;
  tooMuch:boolean; //too much kind of items
begin
  cIndex:=customerIndex(IDInput.Text);
  iIndex:=itemIndex(itemCodeInput.Text);
  quantity:=strToInt(quantityInput.Text);
  if cIndex<>0 then
    if iIndex<>0 then
      if quantity<=0 then
        showMessage(langText[lang].atLeastOneMsg) //buy <1 amount
      else
      begin
        //enough information
        price:=strToFloat(copy(priceText.Caption, 2,
               length(priceText.Caption)-1)); //to ignore the "$"
        if customer[cIndex].cls='SS' then
          price:=simpleRoundTo(price*0.9, -1); //Staff has 10% discount
        if customer[cIndex].money<price then
          showMessage(langText[lang].notEnoughMoneyMsg+ln+langText[lang].youNowHaveMsg+' $'+
                      floatToStr(customer[cIndex].money)+ln+langText[lang].itCostsMsg+' $'+
                      floatToStr(price)) //not enough money
        else if item[iIndex].quantity<quantity then
          showMessage(langText[lang].notEnoughGoodsMsg+ln+langText[lang].weNowHaveMsg+' '+
                      intToStr(item[iIndex].quantity)) //not enough quantity
        else
        begin
          tooMuch:=true;
          if detailCount+1>maxItemOnce then
          begin
            for i:=1 to detailCount do
              if item[iIndex].itemCode=detail[i].itemCode then
              begin
                tooMuch:=false;
              end; //of if

            if tooMuch then
            begin
              showMessage(langText[lang].maxNumberOnceMsg);
              exit;
            end; //of if
          end; //of if

          //can buy
          customer[cIndex].money:=customer[cIndex].money-price;
          item[iIndex].quantity:=item[iIndex].quantity-quantity;

          //reset
          confirmBt.Enabled:=true;
          IDInput.Enabled:=false;
          itemCodeInput.Text:='';
          quantityInput.Text:='1';
          quantityInput.Enabled:=false;
          itemNameText.Caption:='';
          priceText.Caption:='';
          itemPriceOne.Caption:='';
          itemCodeInput.setFocus;
          addToBrought(iIndex, quantity,
                       floatToStr(price));


        end; //of if
      end //of if
    else
    begin
      //not exist item
      showMessage(langText[lang].itemCodeErrMsg);
      itemCodeInput.setFocus;
    end //of if
  else
  begin
    //not exist user
    showMessage(langText[lang].customerIDErrMsg);
    if IDInput.Enabled then
      IDInput.setFocus
    else
      itemCodeInput.setFocus;
  end; //of if
end;

procedure TmainForm.amountWriteOffKeyPress(Sender: TObject; var Key: char);
begin
  writeOffKeyPress(key);
end;

procedure TmainForm.buyShow(Sender:TObject);
begin
  if IDInput.Enabled then
    IDInput.setFocus
  else
    itemCodeInput.setFocus;
end;

procedure TmainForm.cancelBtClick(Sender:TObject);
begin
  cancelBuying;
end;

procedure TmainForm.confirmAddValueClick(Sender: TObject);
var
  index:integer;
  moneyToAdd:real;
begin
  index:=customerIndex(upperCase(IDInputAddValue.Text));
  if index<>0 then
  begin
    moneyToAdd:=strToFloat(moneyAddValue.Text);
    if moneyToAdd<=0 then
      showMessage(langText[lang].moneyToAddMsg)
    else if moneyToAdd>999.9-customer[index].money then
      showMessage(langText[lang].tooMuchAmountMsg+' '+
                 floatToStr(999.9-customer[index].money))
    else
    begin
      //Valid value
      customer[index].money:=customer[index].money+moneyToAdd;
      saveData;
      readInData;
      showMessage(langText[lang].successMsg);

      //reset
      IDInputAddValue.Text:='';
      moneyAddValue.Text:='0';
      IDInputAddValue.SetFocus;
    end; //of if
  end
  else
  begin
    showMessage(langText[lang].customerIDErrMsg);
  end; //of if
end;



procedure TmainForm.confirmBtClick(Sender:TObject);
var
  i:integer;
begin
  //detail
  n_transaction:=n_transaction+1;
  {
  addZero(intToStr(year), 2);
  addZero(intToStr(month), 2);
  addZero(intToStr(day), 2);
  }
  transaction[n_transaction].customerID:=upperCase(IDInput.Text);
  transaction[n_transaction].tranAmount:=strToFloat(copy(totalPriceText.Caption, 9, length(totalPriceText.Caption)-8));
  transaction[n_transaction].tranId:=addZero(intToStr(n_transaction), 4);
  transaction[n_transaction].tranDate:=today;
  for i:=1 to detailCount do
  begin
    transaction[n_transaction].tranDetailItem[i].itemCode:=detail[i].itemCode;
    transaction[n_transaction].tranDetailItem[i].quantity:=detail[i].quantity;
  end; //of for
  transaction[n_transaction].n_detail:=detailCount;




  saveData;
  detailCount:=0; //reset detail

  IDInput.Text:='';
  itemCodeInput.Text:='';
  quantityInput.Text:='1';
  quantityInput.Enabled:=false;
  itemNameText.Caption:='';
  priceText.Caption:='';
  itemPriceOne.Caption:='';
  confirmBt.Enabled:=false;
  IDInput.Enabled:=true;
  IDInput.setFocus;
  brought.RowCount:=1;
  brought.Clean;
  totalPriceText.Caption:='';

  showMessage(langText[lang].successMsg);

end;

procedure TmainForm.confirmPurchaseBtClick(Sender:TObject);
var
  index:integer;
begin
  index:=itemIndex(itemCodePurchase.Text);
  if index<>0 then
  begin
    //item found
    if item[index].quantity+strToInt(quantityPurchase.Text)>99 then
      showMessage(langText[lang].tooMuchAmountMsg+' '+intToStr(99-item[index].quantity))
    else
    begin
      savePurchase(getPurchaseCode, itemIndex(itemCodePurchase.Text),
      strToInt(quantityPurchase.Text));

      item[itemIndex(itemCodePurchase.Text)].quantity:=
      item[itemIndex(itemCodePurchase.Text)].quantity+
      strToInt(quantityPurchase.Text);

      saveData;
      itemCodePurchase.Text:='';
      quantityPurchase.Text:='0';
      itemCodePurchase.setFocus;
    end; //of if
  end
  else
  begin
    showMessage(langText[lang].itemCodeErrMsg);
    itemCodePurchase.setFocus;
  end; //of if
end;

procedure TmainForm.confirmWriteOffClick(Sender: TObject);
var
  index, amount:integer;
begin
  index:=itemIndex(itemCodeWriteOffInput.Text);
  if index<>0 then
  begin
    amount:=strToInt(amountWriteOff.Text);
    if amount>item[index].quantity then
      showMessage(langText[lang].notEnoughGoodsMsg)
    else if amount<=0 then
      showMessage(langText[lang].writeOffMsg)
    else
    begin
      item[index].quantity:=item[index].quantity-amount;
      saveData;

      //reset
      itemCodeWriteOffInput.Text:='';
      amountWriteOff.Text:='0';
      itemCodeWriteOffInput.SetFocus;

      showMessage(langText[lang].successMsg);
    end; //of if
  end
  else
    showMessage(langText[lang].itemCodeErrMsg);
end;

procedure TmainForm.deleteBtClick(Sender:TObject);
begin
  //deleteItem;
end;

procedure TmainForm.deleteItemIndexKeyPress(Sender:TObject; var Key:char);
begin
  if ord(key)=13 then //Enter key pressed
    //deleteItem;
end;

procedure TmainForm.IDInputAddValueChange(Sender: TObject);
var
  index:integer;
  temp:string;
begin
  if length(IDInputAddValue.Text)=8 then
  begin
    index:=customerIndex(upperCase(IDInputAddValue.Text));
    confirmAddValue.Enabled:=index<>0;
    moneyAddValue.Enabled:=index<>0;
    if index<>0 then
    begin
      temp:=langText[lang].userName+customer[index].name;
      if customer[index].cls='SS' then
        temp:=temp+'('+langText[lang].staff+')'+ln
      else
        temp:=temp+customer[index].cls+'('+customer[index].clsNo+')'+ln;
      temp:=temp+langText[lang].money+' $'+floatToStr(customer[index].money)+ln;
      temp:=temp+langText[lang].addValueMost+' ';
      temp:=temp+formatDp(floatToStr(999.9-customer[index].money), 1);

      userInfoAddValue.Caption:=temp;
    end
    else
    begin
      userInfoAddValue.Caption:='';
    end; //of if
  end
  else
  begin
    confirmAddValue.Enabled:=false;
    moneyAddValue.Enabled:=false;
    userInfoAddValue.Caption:='';
  end; //of if
end;

procedure TmainForm.IDInputAddValueKeyPress(Sender: TObject; var Key: char);
begin
  addValueKeyPress(key);
end;

procedure TmainForm.itemCodeInputKeyPress(Sender:TObject; var Key:char);
begin
  buyKeyPress(key);
end;

procedure TmainForm.itemCodePurchaseKeyPress(Sender:TObject; var Key:char);
begin
  supplierKeyPress(key);
end;

procedure TmainForm.itemCodeWriteOffInputChange(Sender: TObject);
var
  itemCode, temp:string;
  index:integer;
begin
  itemCode:=uppercase(itemCodeWriteOffInput.Text);
  temp:='';
  index:=itemIndex(itemCode);
  if index<>0 then
  begin
    temp:=temp+langText[lang].itemName+' '+item[index].itemName+ln;
    temp:=temp+langText[lang].itemQuantityInStock+' '+
          intToStr(item[index].quantity)+ln;
    temp:=temp+langText[lang].writeOffAtMost+' ';
    temp:=temp+intToStr(item[index].quantity);
  end; //of if
  itemInfoWriteOff.Caption:=temp;
  amountWriteOff.Enabled:=index<>0;
  confirmWriteOff.Enabled:=index<>0;
end;

procedure TmainForm.itemCodeWriteOffInputKeyPress(Sender: TObject; var Key: char
  );
begin
  writeOffKeyPress(key);
end;

procedure TmainForm.languageChange(Sender: TObject);
begin
  lang:=language.ItemIndex;
  loadLanguage;
  saveData;
end;

procedure TmainForm.moneyAddValueKeyPress(Sender: TObject; var Key: char);
begin
  addValueKeyPress(key);
end;

procedure TmainForm.quantityInputKeyPress(Sender:TObject; var Key:char);
begin
  buyKeyPress(key);
end;

procedure TmainForm.quantityPurchaseChange(Sender:TObject);
var
  index:integer;
  totalCost:real;
begin
  if itemInfoPurchase.Caption<>'' then //to make sure item exists
  begin
    if quantityPurchase.Text<>'' then
    begin
      index:=itemIndex(itemCodePurchase.Text); //not equal to 0 because item exists
      if (strToInt(quantityPurchase.Text)>0) and (strToInt(quantityPurchase.Text)<=(99-item[index].quantity)) then //to make sure quantity is valid
      begin
        totalCost:=item[index].cost*strToInt(quantityPurchase.Text);
        costPurchase.Caption:=langText[lang].totalCost+' '+floatToStr(totalCost);
        confirmPurchaseBt.Enabled:=true;
      end //of if
      else
      begin
        confirmPurchaseBt.Enabled:=false;
        costPurchase.Caption:='';
      end; //of if
    end
    else
    begin
      costPurchase.Caption:='';
      confirmPurchaseBt.Enabled:=false;
    end; //of if
  end //of if
  else
  begin
    confirmPurchaseBt.Enabled:=false;
    costPurchase.Caption:='';
  end; //of if
end;

procedure TmainForm.itemCodePurchaseChange(Sender:TObject);
var
  index:integer;
begin
  if length(itemCodePurchase.Text)=4 then
  begin
    //show items infomation
    index:=itemIndex(itemCodePurchase.Text);
    if index<>0 then
    begin
      //item found
      if item[index].quantity=99 then
        itemInfoPurchase.Caption:=langText[lang].itemName+' '+item[index].itemName+ln+
                                  langText[lang].supplier+' '+item[index].supplier+ln+
                                  langText[lang].itemQuantityInStock+' '+intToStr(item[index].quantity)+' ('+langText[lang].full+')'+ln+
                                  langText[lang].purchaseCostPerItem+' '+floatToStr(item[index].cost)+ln+
                                  langText[lang].cannotBuy
      else
        itemInfoPurchase.Caption:=langText[lang].itemName+' '+item[index].itemName+ln+
                                  langText[lang].supplier+' '+item[index].supplier+ln+
                                  langText[lang].itemQuantityInStock+' '+intToStr(item[index].quantity)+ln+
                                  langText[lang].purchaseCostPerItem+' '+floatToStr(item[index].cost)+ln+
                                  langText[lang].canBuy+' 1-'+intToStr(99-item[index].quantity);
    end //of if
    else
    begin
      itemInfoPurchase.Caption:='';
      confirmPurchaseBt.Enabled:=false;
      costPurchase.Caption:='';
    end
  end //of if
  else
  begin
    itemInfoPurchase.Caption:='';
    confirmPurchaseBt.Enabled:=false;
    costPurchase.Caption:='';
  end; //of if
end;

procedure TmainForm.FormResize(Sender:TObject);
begin
  //resize is not allowed
  mainForm.Width:=pc.Width;
  mainForm.Height:=pc.Height;
end;

procedure TmainForm.IDInputChange(Sender:TObject);
begin
  customerNameText.Caption:='';
  if showUserInfo(upperCase(IDInput.Text)) then
    {can have code here};
end;

procedure TmainForm.IDInputKeyPress(Sender:TObject; var Key: char);
begin
  buyKeyPress(key);
end;

procedure TmainForm.itemCodeInputChange(Sender:TObject);
begin
  if length(itemCodeInput.Text)=4 then
  begin
    showItemInfo(itemCodeInput.Text);
    //if item exists, then enable quantity input
    quantityInput.Enabled:=itemIndex(itemCodeInput.Text)<>0;

    if itemIndex(itemCodeInput.Text)<>0 then
      {can have code here};

    if itemIndex(itemCodeInput.Text)<>0 then
    begin
      showPrice(itemIndex(itemCodeInput.Text),
                strToInt(quantityInput.Text));
    end; //of if
  end
  else
  begin
    itemNameText.Caption:='';
    quantityInput.Enabled:=false;
    priceText.Caption:='';
    itemPriceOne.Caption:='';
  end; //of if
end;

procedure TmainForm.pcChange(Sender:TObject);
begin
  if confirmBt.Enabled then
  begin
    //someone add item but did not confirm or cancel
    showMessage(langText[lang].changePageErrMsg);
    pc.ActivePage:=buy;
    itemCodeInput.SetFocus;
  end; //of if

  if pc.ActivePage=buy then
    if IDInput.Enabled then
      IDInput.SetFocus
    else
      itemCodeInput.SetFocus
  else if pc.ActivePage=pcItems then
    select.SetFocus
  else if pc.ActivePage=reports then
    reportList.SetFocus
  else if pc.ActivePage=supplier then
    itemCodePurchase.SetFocus
  else if pc.ActivePage=addValue then
    IDInputAddValue.SetFocus
  else if pc.ActivePage=writeOff then
    itemCodeWriteOffInput.SetFocus;
end;

procedure TmainForm.quantityInputChange(Sender:TObject);
begin
  if (quantityInput.Text<>'0')and(quantityInput.Text<>'') then
    showPrice(itemIndex(upperCase(itemCodeInput.Text)),
              strToInt(quantityInput.Text))
  else
    priceText.Caption:='';
end;

procedure TmainForm.quantityPurchaseKeyPress(Sender:TObject; var Key:char);
begin
  supplierKeyPress(key);
end;

procedure TmainForm.reportBtClick(Sender:TObject);
begin
  //Report button clicked
  case reportList.itemIndex of
    0:reportPurchaseClass;
    1:reportPurchaseGender;
    2:reportTopTenPurchase;
    3:reportFrequency;
  else
    showMessage(langText[lang].adminMsg);
  end;
end;

procedure TmainForm.selectChange(Sender: TObject);
begin
  showItems(select.itemIndex);
end;

end.

