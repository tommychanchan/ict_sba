unit utils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, StdCtrls,
  ExtCtrls, Grids;

const
  ln = {$IFDEF LINUX} AnsiChar(#10) {$ENDIF}
       {$IFDEF MSWINDOWS} AnsiString(#13#10) {$ENDIF}; //ln: line break
  max_item=1000;
  max_customer=1000;
  maxItemOnce=50; //max different number of items buy once
  maxTran=100000;

procedure readInData;
procedure saveData;
procedure showItems(index:integer);
procedure showUserInfo(s:string);
procedure showItemInfo(s:string);
function customerIndex(s:string):integer;
function itemIndex(s:string):integer;
function toUpper(s:string):string;
procedure showPrice(index, n:integer);
procedure addToBrought(iIndex, quantity:integer; price: string);
procedure buyKeyPress(key:char); //key press in page "buy"
procedure deleteItem;


implementation
uses
  unit1;



procedure readInData;
var
  f:text;
  i, count:integer;
  temp:char;
begin
  //item
  assign(f, '../../DB/items.txt');
  reset(f);
  i:=0;
  while not eof(f) do
  begin
    i:=i+1;
    read(f, item[i].itemCode);
    read(f, item[i].itemName);
    read(f, item[i].supplier);
    read(f, item[i].cost);
    read(f, item[i].price);
    readln(f, item[i].quantity);
  end; {of while}
  close(f);
  n_item:=i;

  //customer
  assign(f, '../../DB/customer.txt');
  reset(f);
  i:=0;
  while not eof(f) do
  begin
    i:=i+1;
    read(f, customer[i].code);
    read(f, customer[i].cls);
    read(f, customer[i].clsNo);
    read(f, customer[i].name);
    read(f, customer[i].gender);
    read(f, temp);
    readln(f, customer[i].money);
  end; //of while
  close(f);
  n_customer:=i;

  assign(f, '../../DB/transact.txt');
  reset(f);
  i:=0;
  while not eof(f) do
  begin
    i:=i+1;
    read(f, transaction[i].tranId);
    read(f, temp);
    read(f, transaction[i].tranDate);
    read(f, temp);
    read(f, transaction[i].customerID);
    read(f, transaction[i].tranAmount);
    read(f, temp);
    count:=0;
    while not eoln(f) do
    begin
      count:=count+1;
      read(f, transaction[i].tranDetailItem[count].itemCode);
      read(f, temp);
      read(f, transaction[i].tranDetailItem[count].quantity);
      read(f, temp);
    end; //of while
    readln(f);
  end; //of while
  close(f);
end; //of procedure

procedure saveData;
var
  f:text;
  i:integer;
begin
  //item
  assign(f, '../../DB/items.txt');
  rewrite(f);
  for i:=1 to n_item do
  begin
    write(f, item[i].itemCode);
    write(f, item[i].itemName);
    write(f, item[i].supplier);
    write(f, item[i].cost:5:1);
    write(f, item[i].price:5:1);
    writeln(f, item[i].quantity:3);
  end; //of for
  close(f);

  //customer
  assign(f, '../../DB/customer.txt');
  rewrite(f);
  for i:=1 to n_customer do
  begin
    write(f, customer[i].code);
    write(f, customer[i].cls);
    write(f, customer[i].clsNo);
    write(f, customer[i].name);
    write(f, customer[i].gender);
    writeln(f, customer[i].money:6:1);
  end; //of for
  close(f);
end; //of procedure

procedure showItems(index:integer);
var
  letter:char;
  i, count:integer;
begin
  //reset
  mainForm.enquiry.Clean;
  mainForm.enquiry.RowCount:=1;

  case index of
    0:letter:='S';
    1:letter:='R';
    2:letter:='F';
    3:letter:='D';
  end; //of case

  count:=0;
  for i:=1 to n_item do
    if item[i].itemCode[1]=letter then
    begin
      count:=count+1;
      mainForm.enquiry.RowCount:=mainForm.enquiry.RowCount+1;
      mainForm.enquiry.Cells[0, count]:=item[i].itemCode;
      mainForm.enquiry.Cells[1, count]:=item[i].itemName;
      mainForm.enquiry.Cells[2, count]:=floatToStr(item[i].price);
      mainForm.enquiry.Cells[3, count]:=intToStr(item[i].quantity);
    end; //of if


end; //of procedure

procedure showUserInfo(s:string);
var
  index:integer;
begin
  index:=customerIndex(s);
  if (index<>0) then
  begin
    //user found
    mainForm.customerNameText.Caption:='Welcome, '+customer[index].name; //Show name

    if customer[index].cls<>'SS' then
      mainForm.customerNameText.Caption:=
      mainForm.customerNameText.Caption+'('+customer[index].cls+')'
    else
      mainForm.customerNameText.Caption:=
      mainForm.customerNameText.Caption+'(Staff)';

    mainForm.customerNameText.Caption:=
    mainForm.customerNameText.Caption+ln+'  Money: $'+
    floatToStr(customer[index].money); //Show Money
  end; //of if
end; //of procedure

procedure showItemInfo(s:string);
var
  index:integer;
begin
  index:=itemIndex(s);
  if (index<>0) then
  begin
    //item found
    mainForm.itemNameText.Caption:=item[index].itemName+' ('+
                                   intToStr(item[index].quantity)+')';
    mainForm.itemPriceOne.Caption:='$'+floatToStr(item[index].price);
  end; //of if
end; //of procedure

function customerIndex(s:string):integer;
var
  i, re:integer;
begin
  s:=toUpper(s);
  re:=0; //return 0 if not found
  for i:=1 to n_customer do
    if (customer[i].code=s) then
    begin
      re:=i;
      break; //exit for loop
    end; //of if
  customerIndex:=re;
end; //of function

function itemIndex(s:string):integer;
var
  i, re:integer;
begin
  s:=toUpper(s);
  re:=0; //return 0 if not found
  for i:=1 to n_item do
    if (item[i].itemCode=s) then
    begin
      re:=i;
      break; //exit for loop
    end; //of if
  itemIndex:=re;
end; //of function

function toUpper(s:string):string;
var
  i:integer;
  re:string;
begin
  re:=s;
  for i:=1 to length(re) do
    if (ord(re[i])>=97)and(ord(re[i])<=122) then
      re[i]:=chr(ord(re[i])-32);
  toUpper:=re;
end; //of function

procedure showPrice(index, n:integer);
begin
  mainForm.priceText.Caption:='$'+floatToStr(item[index].price*n);
end; //of procedure

procedure addToBrought(iIndex, quantity:integer; price: string);
var
  row, i, j:integer;
  sum:real;
  added:boolean;
begin
  row:=mainForm.brought.RowCount;
  added:=false; //ini
  for i:=1 to row-1 do
    if (item[iIndex].itemName=mainForm.brought.Cells[3, i]) then
    begin
      //repeated item

      mainForm.brought.Cells[2, i]:=
      intToStr(strToInt(mainForm.brought.Cells[2, i])+
      quantity); //add quantity

      mainForm.brought.Cells[4, i]:=
      floatToStr(strToFloat(mainForm.brought.Cells[4, i])+
      strToFloat(price)); //add price

      added:=true;
      sum:=0;

      for j:=1 to row-1 do
        sum:=sum+strToFloat(mainForm.brought.Cells[4, j]);

      for j:=1 to detailCount do
        if (detail[j].itemCode=item[iIndex].itemCode) then
          detail[j].quantity:=detail[j].quantity+quantity;

    end; //of if

  if not added then
  begin
    //not repeat item
    mainForm.brought.RowCount:=row+1;

    mainForm.brought.Cells[0, row]:=intToStr(row);
    mainForm.brought.Cells[1, row]:=item[iIndex].itemCode;
    mainForm.brought.Cells[2, row]:=intToStr(quantity);
    mainForm.brought.Cells[3, row]:=item[iIndex].itemName;
    mainForm.brought.Cells[4, row]:=price;
    sum:=0;

    for j:=1 to row do
      sum:=sum+strToFloat(mainForm.brought.Cells[4, j]);

    detailCount:=detailCount+1;
    detail[detailCount].itemCode:=item[iIndex].itemCode;
    detail[detailCount].quantity:=quantity;
  end; //of if



  mainForm.totalPriceText.Caption:='Total: $'+floatToStr(sum);
  mainForm.deleteBt.Enabled:=mainForm.brought.RowCount>1;
  mainForm.confirmBt.Enabled:=mainForm.brought.RowCount>1;
end; //of procedure

procedure buyKeyPress(key:char);
begin
  {
  case ord(key) of

  end; //of case
  }
end; //of procedure

procedure deleteItem;
var
  index, cIndex, iIndex, quantity, i:integer;
  price, sum:real;
begin
  index:=strToInt(mainForm.deleteItemIndex.Text);
  cIndex:=customerIndex(mainForm.IDInput.Text);
  iIndex:=itemIndex(mainForm.brought.Cells[1, index]);
  quantity:=strToInt(mainForm.brought.Cells[2, index]);
  price:=strToFloat(mainForm.brought.Cells[4, index]);

  customer[cIndex].money:=customer[cIndex].money+price;
  item[iIndex].quantity:=item[iIndex].quantity+quantity;

  for i:=index to mainForm.brought.rowCount-2 do
  begin
    mainForm.brought.Cells[1, i]:=mainForm.brought.Cells[1, i+1];
    mainForm.brought.Cells[2, i]:=mainForm.brought.Cells[2, i+1];
    mainForm.brought.Cells[3, i]:=mainForm.brought.Cells[3, i+1];
    mainForm.brought.Cells[4, i]:=mainForm.brought.Cells[4, i+1];
  end; //of for

  mainForm.brought.rowCount:=mainForm.brought.rowCount-1;

  sum:=0;
  for i:=1 to mainForm.brought.rowCount-1 do
    sum:=sum+strToFloat(mainForm.brought.Cells[4, i]);

  mainForm.totalPriceText.Caption:='Total: $'+floatToStr(sum);


  mainForm.deleteBt.Enabled:=mainForm.brought.RowCount>1;
  mainForm.confirmBt.Enabled:=mainForm.brought.RowCount>1;
  mainForm.itemCodeInput.setFocus;
end; //of procedure



end.

