unit utils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
  ln = {$IFDEF LINUX} AnsiChar(#10) {$ENDIF}
       {$IFDEF MSWINDOWS} AnsiString(#13#10) {$ENDIF}; {ln: line break}
  max_item=1000;

procedure readInData;
procedure showItems(index:integer);


implementation
uses
  unit1;



procedure readInData;
var
  f:text;
  i:integer;
begin
  assign(f, '../../DB/items.txt');
  reset(f);
  i:=0;
  while not eof(f) do
  begin
    i:=i+1;
    read(f, item[i].item_code);
    read(f, item[i].item_name);
    read(f, item[i].supplier);
    read(f, item[i].cost);
    read(f, item[i].price);
    readln(f, item[i].quantity);
  end; {of while}
  close(f);
  n_item:=i;
end;

procedure showItems(index:integer);
var
  letter:char;
  i, count:integer;
begin
  {reset}
  mainForm.enquiry.Clean;
  mainForm.enquiry.RowCount:=1;

  case index of
    0:letter:='S';
    1:letter:='R';
    2:letter:='F';
    3:letter:='D';
  end; {of case}

  count:=0;
  for i:=1 to n_item do
    if item[i].item_code[1]=letter then
    begin
      count:=count+1;
      mainForm.enquiry.RowCount:=mainForm.enquiry.RowCount+1;
      mainForm.enquiry.Cells[0, count]:=item[i].item_code;
      mainForm.enquiry.Cells[1, count]:=item[i].item_name;
      mainForm.enquiry.Cells[2, count]:=floatToStr(item[i].price);
      mainForm.enquiry.Cells[3, count]:=intToStr(item[i].quantity);
    end; {of if}


end;


end.

