unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, StdCtrls,
  ExtCtrls, Grids, utils;


type

  { TmainForm }

  TmainForm = class(TForm)
    select: TComboBox;
    pc: TPageControl;
    pcItems: TTabSheet;
    enquiry: TStringGrid;
    TabSheet2: TTabSheet;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure selectChange(Sender: TObject);
  private

  public

  end;

  string4=string[4];
  string25=string[25];
  item_record=record
    item_code:string4;
    item_name:string25;
    supplier:string25;
    cost:real;
    price:real;
    quantity:integer;
  end;

  item_array=array [1..max_item] of item_record;

var
  mainForm: TMainForm;
  item:item_array;
  n_item:integer;


implementation

{$R *.lfm}

{ TmainForm }

procedure TmainForm.FormActivate(Sender: TObject);
begin
  readInData;

  showItems(select.itemIndex);
end;

procedure TmainForm.FormResize(Sender: TObject);
begin
  mainForm.Width:=700;

end;

procedure TmainForm.selectChange(Sender: TObject);
begin
  showItems(select.itemIndex);
end;

end.

