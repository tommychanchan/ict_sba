unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, StdCtrls,
  ExtCtrls, Grids, utils, Math;


type

  { TmainForm }

  TmainForm = class(TForm)
    addItemBt: TButton;
    cancelBt: TButton;
    confirmBt: TButton;
    Label5: TLabel;
    itemPriceOne: TLabel;
    totalPriceText: TLabel;
    priceText: TLabel;
    quantityInput: TEdit;
    itemCodeInput: TEdit;
    IDInput: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    itemNameText: TLabel;
    customerNameText: TLabel;
    Label4: TLabel;
    select: TComboBox;
    pc: TPageControl;
    pcItems: TTabSheet;
    enquiry: TStringGrid;
    buy: TTabSheet;
    brought: TStringGrid;
    procedure addItemBtClick(Sender: TObject);
    procedure cancelBtClick(Sender: TObject);
    procedure confirmBtClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure IDInputChange(Sender: TObject);
    procedure itemCodeInputChange(Sender: TObject);
    procedure pcChange(Sender: TObject);
    procedure quantityInputChange(Sender: TObject);
    procedure selectChange(Sender: TObject);
  private

  public

  end;

  string2=string[2];
  string4=string[4];
  string8=string[8];
  string20=string[20];
  string25=string[25];
  item_record=record
    itemCode:string4;
    itemName:string25;
    supplier:string25;
    cost:real;
    price:real;
    quantity:integer;
  end;

  customer_record=record
    code:string8;
    cls:string2;
    clsNo:string2;
    name:string20;
    gender:string2;
    money:real;
  end;

  tran_detail=record
    itemCode:string4;
    quantity:integer;
  end;

  item_array=array [1..max_item] of item_record;
  customer_array=array [1..max_customer] of customer_record;

var
  mainForm: TMainForm;
  item:item_array;
  customer:customer_array;
  n_item, n_customer, detailCount:integer;
  //detailCount = number of different items user brought
  detail:array [1..maxItemOnce] of tran_detail;


implementation

{$R *.lfm}

{ TmainForm }

procedure TmainForm.FormActivate(Sender: TObject);
begin
  readInData; //get info in database

  //ini form elements setting
  showItems(select.itemIndex);
  quantityInput.Enabled:=false;
  confirmBt.Enabled:=false;

  detailCount:=0; //ini detail

  pc.ActivePage:=buy; //set ActivePage for page control
  IDInput.setFocus; //focus IDInput
end;

procedure TmainForm.addItemBtClick(Sender: TObject);
var
  cIndex, iIndex, //cIndex=customer index; iIndex=item index.
  quantity, i:integer;
  price:real;
begin
  cIndex:=customerIndex(IDInput.Text);
  iIndex:=itemIndex(itemCodeInput.Text);
  quantity:=strToInt(quantityInput.Text);
  if (cIndex<>0) then
    if (iIndex<>0) then
      if (quantity<=0) then
        showMessage('You must buy at least one.') //buy <1 amount
      else
      begin
        //enough information
        price:=strToFloat(copy(priceText.Caption, 2,
               length(priceText.Caption)-1)); //to ignore the "$"
        if (customer[cIndex].gender[2]='Y') then
          price:=simpleRoundTo(price*0.9, -1); //VIP have 10% discount
        if (customer[cIndex].money<price) then
          showMessage('You have not enough money to buy.') //not enough money
        else if (item[iIndex].quantity<quantity) then
          showMessage('Not enough goods.'+ln+'Now we have '+
                      intToStr(item[iIndex].quantity)) //not enough quantity
        else
        begin
          if (detailCount+1>maxItemOnce) then
            for i:=1 to detailCount do
              if (item[iIndex].itemCode=detail[i].itemCode) then
              begin
                //can have code here
              end
              else
              begin
                showMessage('Max number of different items once.');
                exit;
              end; {of if}
          //can buy
          customer[cIndex].money:=customer[cIndex].money-price;
          item[iIndex].quantity:=item[iIndex].quantity-quantity;

          //reset
          confirmBt.Enabled:=true;
          IDInput.Enabled:=false;
          itemCodeInput.Text:='';
          quantityInput.Text:='1';
          quantityInput.Enabled:=false;
          itemNameText.Caption:='';
          priceText.Caption:='';
          itemPriceOne.Caption:='';
          itemCodeInput.setFocus;
          addToBrought(iIndex, quantity,
                       floatToStr(price));

          showMessage('Added item.'+ln+
                      'Click Confirm button to buy.'+ln+
                      'Click Cancel button to abort.');
        end; {of if}
      end {of if}
    else
    begin
      //not exist item
      showMessage('Item code not found.');
      itemCodeInput.setFocus;
    end {of if}
  else
  begin
    //not exist user
    showMessage('Customer ID not found.');
    IDInput.setFocus;
  end; {of if}
end;

procedure TmainForm.cancelBtClick(Sender: TObject);
var
  i:integer;
begin
  readInData;

  detailCount:=0; //ini detail

  IDInput.Text:='';
  itemCodeInput.Text:='';
  quantityInput.Text:='1';
  quantityInput.Enabled:=false;
  itemNameText.Caption:='';
  priceText.Caption:='';
  itemPriceOne.Caption:='';
  confirmBt.Enabled:=false;
  IDInput.Enabled:=true;
  IDInput.setFocus;
  brought.RowCount:=1;
  brought.Clean;
  totalPriceText.Caption:='';
end;

procedure TmainForm.confirmBtClick(Sender: TObject);
begin
  saveData;
  detailCount:=0; //ini detail

  IDInput.Text:='';
  itemCodeInput.Text:='';
  quantityInput.Text:='1';
  quantityInput.Enabled:=false;
  itemNameText.Caption:='';
  priceText.Caption:='';
  itemPriceOne.Caption:='';
  confirmBt.Enabled:=false;
  IDInput.Enabled:=true;
  IDInput.setFocus;
  brought.RowCount:=1;
  brought.Clean;
  totalPriceText.Caption:='';

  showMessage('Success.');
end;

procedure TmainForm.FormResize(Sender: TObject);
begin
  //resize is not allowed
  mainForm.Width:=700;
  mainForm.Height:=700;
end;

procedure TmainForm.IDInputChange(Sender: TObject);
begin
  if length(IDInput.Text)=8 then
    showUserInfo(toUpper(IDInput.Text))
  else
    customerNameText.Caption:='';
end;

procedure TmainForm.itemCodeInputChange(Sender: TObject);
begin
  if length(itemCodeInput.Text)=4 then
  begin
    showItemInfo(itemCodeInput.Text);
    //if item exists, then enable quantity input
    quantityInput.Enabled:=itemIndex(itemCodeInput.Text)<>0;
    if itemIndex(itemCodeInput.Text)<>0 then
    begin
      showPrice(itemIndex(itemCodeInput.Text),
                strToInt(quantityInput.Text));
    end; {of if}
  end
  else
  begin
    itemNameText.Caption:='';
    quantityInput.Enabled:=false;
    priceText.Caption:='';
    itemPriceOne.Caption:='';
  end;
end;

procedure TmainForm.pcChange(Sender: TObject);
begin
  if (confirmBt.Enabled) then
  begin
    //someone add item but did not confirm or cancel
    showMessage('You must confirm your buying or cancel it.');
    pc.ActivePage:=buy;
  end; {of if}

  if pc.ActivePage=buy then
    IDInput.setFocus
  else if pc.ActivePage=pcItems then
    select.setFocus;
end;

procedure TmainForm.quantityInputChange(Sender: TObject);
begin
  if (quantityInput.Text<>'0')and(quantityInput.Text<>'') then
    showPrice(itemIndex(toUpper(itemCodeInput.Text)),
              strToInt(quantityInput.Text))
  else
    priceText.Caption:='';
end;

procedure TmainForm.selectChange(Sender: TObject);
begin
  showItems(select.itemIndex);
end;

end.

