unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, StdCtrls,
  ExtCtrls, Grids, utils, Math, dos;


type

  { TmainForm }

  TmainForm = class(TForm)
    addItemBt: TButton;
    deleteBt: TButton;
    cancelBt: TButton;
    confirmBt: TButton;
    deleteItemIndex: TEdit;
    Label5: TLabel;
    itemPriceOne: TLabel;
    Label6: TLabel;
    reports: TTabSheet;
    totalPriceText: TLabel;
    priceText: TLabel;
    quantityInput: TEdit;
    itemCodeInput: TEdit;
    IDInput: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    itemNameText: TLabel;
    customerNameText: TLabel;
    Label4: TLabel;
    select: TComboBox;
    pc: TPageControl;
    pcItems: TTabSheet;
    enquiry: TStringGrid;
    buy: TTabSheet;
    brought: TStringGrid;
    procedure addItemBtClick(Sender: TObject);
    procedure buyShow(Sender: TObject);
    procedure cancelBtClick(Sender: TObject);
    procedure confirmBtClick(Sender: TObject);
    procedure deleteBtClick(Sender: TObject);
    procedure deleteItemIndexKeyPress(Sender: TObject; var Key: char);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure IDInputChange(Sender: TObject);
    procedure IDInputKeyPress(Sender: TObject; var Key: char);
    procedure itemCodeInputChange(Sender: TObject);
    procedure pcChange(Sender: TObject);
    procedure quantityInputChange(Sender: TObject);
    procedure selectChange(Sender: TObject);
  private

  public

  end;

  string2=string[2];
  string4=string[4];
  string8=string[8];
  string20=string[20];
  string25=string[25];

  item_record=record
    itemCode:string4;
    itemName:string25;
    supplier:string25;
    cost:real;
    price:real;
    quantity:integer;
  end; //of record

  customer_record=record
    code:string8;
    cls:string2;
    clsNo:string2;
    name:string20;
    gender:string2;
    money:real;
  end; //of record

  tran_detail=record
    itemCode:string4;
    quantity:integer;
  end; //of record

  tran_detail_array=array [1..maxItemOnce] of tran_detail;

  transaction_record=record
    tranId:string4;
    tranDate:string8;
    customerID:string8;
    tranAmount:real;
    n_detail:integer;
    tranDetailItem:tran_detail_array;
  end; //of record


  item_array=array [1..max_item] of item_record;
  customer_array=array [1..max_customer] of customer_record;
  transaction_array=array [1..maxTran] of transaction_record;

var
  mainForm: TMainForm;
  item:item_array;
  customer:customer_array;
  n_item, n_customer, detailCount:integer;
  //detailCount = number of different items user brought
  detail:tran_detail_array;
  transaction:transaction_array;
  n_transaction:integer;


implementation

{$R *.lfm}

{ TmainForm }

procedure TmainForm.FormActivate(Sender: TObject);
begin
  readInData; //get info in database

  //ini form elements setting
  showItems(select.itemIndex);
  quantityInput.Enabled:=false;
  confirmBt.Enabled:=false;

  detailCount:=0; //ini detail

  pc.ActivePage:=buy; //set ActivePage for page control
  IDInput.setFocus; //focus IDInput
end;

procedure TmainForm.addItemBtClick(Sender: TObject);
var
  cIndex, iIndex, //cIndex=customer index; iIndex=item index.
  quantity, i:integer;
  price:real;
begin
  cIndex:=customerIndex(IDInput.Text);
  iIndex:=itemIndex(itemCodeInput.Text);
  quantity:=strToInt(quantityInput.Text);
  if (cIndex<>0) then
    if (iIndex<>0) then
      if (quantity<=0) then
        showMessage('You must buy at least one.') //buy <1 amount
      else
      begin
        //enough information
        price:=strToFloat(copy(priceText.Caption, 2,
               length(priceText.Caption)-1)); //to ignore the "$"
        if (customer[cIndex].cls='SS') then
          price:=simpleRoundTo(price*0.9, -1); //Staff has 10% discount
        if (customer[cIndex].money<price) then
          showMessage('You have not enough money to buy.'+ln+'You have $'+
                      floatToStr(customer[cIndex].money)+
                      ' now.'+ln+'It costs $'+
                      floatToStr(price)) //not enough money
        else if (item[iIndex].quantity<quantity) then
          showMessage('Not enough goods.'+ln+'Now we have '+
                      intToStr(item[iIndex].quantity)) //not enough quantity
        else
        begin
          if (detailCount+1>maxItemOnce) then
            for i:=1 to detailCount do
              if (item[iIndex].itemCode=detail[i].itemCode) then
              begin
                //can have code here
              end
              else
              begin
                showMessage('Max number of different items once.');
                exit;
              end; {of if}
          //can buy
          customer[cIndex].money:=customer[cIndex].money-price;
          item[iIndex].quantity:=item[iIndex].quantity-quantity;

          //reset
          confirmBt.Enabled:=true;
          IDInput.Enabled:=false;
          itemCodeInput.Text:='';
          quantityInput.Text:='1';
          quantityInput.Enabled:=false;
          itemNameText.Caption:='';
          priceText.Caption:='';
          itemPriceOne.Caption:='';
          itemCodeInput.setFocus;
          addToBrought(iIndex, quantity,
                       floatToStr(price));


        end; //of if
      end //of if
    else
    begin
      //not exist item
      showMessage('Item code not found.');
      itemCodeInput.setFocus;
    end //of if
  else
  begin
    //not exist user
    showMessage('Customer ID not found.');
    if IDInput.Enabled then
      IDInput.setFocus
    else
      itemCodeInput.setFocus;
  end; //of if
end;

procedure TmainForm.buyShow(Sender: TObject);
begin
  if IDInput.Enabled then
    IDInput.setFocus
  else
    itemCodeInput.setFocus;
end;

procedure TmainForm.cancelBtClick(Sender: TObject);
var
  i:integer;
begin
  readInData;

  detailCount:=0; //ini detail

  IDInput.Text:='';
  itemCodeInput.Text:='';
  quantityInput.Text:='1';
  quantityInput.Enabled:=false;
  itemNameText.Caption:='';
  priceText.Caption:='';
  itemPriceOne.Caption:='';
  confirmBt.Enabled:=false;
  IDInput.Enabled:=true;
  IDInput.setFocus;
  brought.RowCount:=1;
  brought.Clean;
  totalPriceText.Caption:='';
  n_transaction:=0;
end;



procedure TmainForm.confirmBtClick(Sender: TObject);
var
  i:integer;
  year,month,day,wDay:word;
begin
  //detail
  GetDate(year,month,day,wDay);
  n_transaction:=n_transaction+1;
  {
  addZero(intToStr(year), 2);
  addZero(intToStr(month), 2);
  addZero(intToStr(day), 2);
  }
  transaction[n_transaction].customerID:=toUpper(IDInput.Text);
  transaction[n_transaction].tranAmount:=strToFloat(copy(totalPriceText.Caption, 9, length(totalPriceText.Caption)-8));
  transaction[n_transaction].tranId:=addZero(intToStr(n_transaction), 4);
  transaction[n_transaction].tranDate:=addZero(year)+addZero(intToStr(month), 2)+addZero(intToStr(day), 2);
  for i:=1 to detailCount do
  begin
    transaction[n_transaction].tranDetailItem[i].itemCode:=detail[i].itemCode;
    transaction[n_transaction].tranDetailItem[i].quantity:=detail[i].quantity;
  end; //of for
  transaction[n_transaction].n_detail:=detailCount;




  saveData;
  detailCount:=0; //ini detail

  IDInput.Text:='';
  itemCodeInput.Text:='';
  quantityInput.Text:='1';
  quantityInput.Enabled:=false;
  itemNameText.Caption:='';
  priceText.Caption:='';
  itemPriceOne.Caption:='';
  confirmBt.Enabled:=false;
  IDInput.Enabled:=true;
  IDInput.setFocus;
  brought.RowCount:=1;
  brought.Clean;
  totalPriceText.Caption:='';
  n_transaction:=0;

  showMessage('Success.');

end;

procedure TmainForm.deleteBtClick(Sender: TObject);
begin
  //deleteItem;
end;

procedure TmainForm.deleteItemIndexKeyPress(Sender: TObject; var Key: char);
begin
  if (ord(key)=13) then //Enter key pressed
    //deleteItem;
end;

procedure TmainForm.FormResize(Sender: TObject);
begin
  //resize is not allowed
  mainForm.Width:=700;
  mainForm.Height:=700;
end;

procedure TmainForm.IDInputChange(Sender: TObject);
begin
  if length(IDInput.Text)=8 then
    showUserInfo(toUpper(IDInput.Text))
  else
    customerNameText.Caption:='';
end;

procedure TmainForm.IDInputKeyPress(Sender: TObject; var Key: char);
begin
  buyKeyPress(key);
end;

procedure TmainForm.itemCodeInputChange(Sender: TObject);
begin
  if length(itemCodeInput.Text)=4 then
  begin
    showItemInfo(itemCodeInput.Text);
    //if item exists, then enable quantity input
    quantityInput.Enabled:=itemIndex(itemCodeInput.Text)<>0;
    if itemIndex(itemCodeInput.Text)<>0 then
    begin
      showPrice(itemIndex(itemCodeInput.Text),
                strToInt(quantityInput.Text));
    end; //of if
  end
  else
  begin
    itemNameText.Caption:='';
    quantityInput.Enabled:=false;
    priceText.Caption:='';
    itemPriceOne.Caption:='';
  end;
end;

procedure TmainForm.pcChange(Sender: TObject);
begin
  if (confirmBt.Enabled) then
  begin
    //someone add item but did not confirm or cancel
    showMessage('You must confirm your buying or cancel it.');
    pc.ActivePage:=buy;
  end; //of if

  if pc.ActivePage=buy then
    if IDInput.Enabled then
      IDInput.setFocus
    else
      itemCodeInput.setFocus
  else if pc.ActivePage=pcItems then
    select.setFocus;
end;

procedure TmainForm.quantityInputChange(Sender: TObject);
begin
  if (quantityInput.Text<>'0')and(quantityInput.Text<>'') then
    showPrice(itemIndex(toUpper(itemCodeInput.Text)),
              strToInt(quantityInput.Text))
  else
    priceText.Caption:='';
end;

procedure TmainForm.selectChange(Sender: TObject);
begin
  showItems(select.itemIndex);
end;

end.

